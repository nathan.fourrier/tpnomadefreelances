// Import
const fs = require('fs');

// directory path
const dir = `${__dirname}/pdf/`;

// Get size of a file in KiB
function getSize(fileName) {
  return Math.round(fs.statSync(dir + fileName).size / 1024);
}

// Get files list in the pdf folder
function getFiles() {
  const files = fs.readdirSync(dir);
  return files.map((file) => ({ name: file, size: getSize(file) }));
}

exports.default = getFiles;
