# Pdf manager for Nomades
> A cloud pdf sharing plateform in nodejs

## General info
This project is free to use, put your startup name into it and enjoy the earned money (or pay me a coffee with it).

### Home page

![Upload home page](./img/screenshotHome.png)

To upload file(s) you just need to drag and drop (or browse) it.
Check the list and click upload.

### Upload page

![Upload page screenshot](./img/screenshotUpload.png)

## Technologies
* Nodejs
* express
* html
* javascript

## Setup

First you need to clone this repo with the following command

`git clone https://gitlab.com/nathan.fourrier/tpnomadefreelances`

Change directory and install npm packages

`cd tpnomadefreelances`

`npm i`

or manually install dependencies:

`npm i express dotenv ejs fs multer`

`node app.js`

## Config
Your can edit __.env__ file depending to your needs.

Here is an example of config

`SERVER_PORT=8080`

`STARTUP_NAME=NOMADE freelances`

## Status
Project is: _ended_

## Contact
Created by Nathan FOURRIER - feel free to contact me!
