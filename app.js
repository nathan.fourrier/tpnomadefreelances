// Imports
require('dotenv').config();
require('ejs');
const fs = require('fs');

// Setup web server
const express = require('express');

const app = express();
app.use(express.static('public'));
app.set('view engine', 'ejs');
app.use(express.urlencoded({ extended: true }));

// Setup Multer to manage files uploads
const multer = require('multer');

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, './pdf');
  },
  filename(req, file, cb) {
    cb(null, file.originalname);
  },
});

const upload = multer({ storage });

// Require filesList to parse pdf's list directory
const files = require('./filesList.js');

// ROUTES OF THE SERVER
// Redirect root route
app.get('/', (req, res) => {
  res.redirect('/home');
});

// GET home route
app.get('/home', (req, res) => {
  const filesList = files.default();
  if (filesList.length > 0) {
    res.render('home', {
      pageName: 'Home',
      startupName: process.env.STARTUP_NAME,
      files: filesList,
    });
  } else {
    res.redirect('/upload');
  }
});

// GET upload route
app.get('/upload', (req, res) => {
  res.render('upload', {
    pageName: 'Upload',
    startupName: process.env.STARTUP_NAME,
  });
});

// GET specific file
app.get('/pdf/:fileName', (req, res) => {
  const { fileName } = req.params;
  res.sendFile(`${__dirname}/pdf/${fileName}`);
});

// DELETE specific file
app.delete('/pdf/:fileName', (req, res) => {
  const { fileName } = req.params;
  let status = 0;

  // Delete the file in the pdf folder
  fs.unlink(`${__dirname}/pdf/${fileName}`, (err) => {
    if (err) {
      status = 500;
    } else {
      status = 200;
    }
    res.sendStatus(status);
  });
});

// POST a pdf using multer
app.post('/pdf', upload.single('file'), (req, res) => {
  res.sendStatus(200);
});

// Listen with server to the configured port
const serverPort = process.env.SERVER_PORT;

app.listen(serverPort, () => {
  // eslint-disable-next-line no-console
  console.log(`Server started at http://127.0.0.1:${serverPort}`);
});
