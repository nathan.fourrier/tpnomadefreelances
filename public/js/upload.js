// File list to upload
const filesList = [];

// Regex for checking pdf
const pdfChecker = new RegExp('.*\.pdf$');

// Display file list to upload
function displayFile(filename) {
  const list = document.getElementsByClassName('list-group')[0];

  const fileIl = document.createElement('LI');
  fileIl.classList = 'list-group-item';
  fileIl.innerText = filename;

  list.appendChild(fileIl);
}

// Delete a file from list
function deleteFile(file) {
  fetch(`/pdf/${file}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    },
    body: null,
  })
    .then(() => location.reload())
    .catch(alert);
}

// Adding files to with check list
function addToList(files) {
  // Filter the file list to have only pdf's
  files = files.filter((file) => {
    const ispdf = pdfChecker.test(file.name);
    if (!ispdf) {
      alert(`File ${file.name} is not a pdf !!`);
    }
    return ispdf;
  });

  // Displaying file list
  files.forEach((file) => {
    document.getElementsByClassName('upload-btn')[0].disabled = false;
    filesList.push(file);
    displayFile(file.name);
  });
}

// Manual browse function
function onBrowseChange(event) {
  const files = [];
  for (let i = 0; i < event.target.files.length; i++) {
    files.push(event.target.files[i]);
  }
  addToList(files);
}

// Drag and Drop functions

// Change color of drop-zone when a file dragOver it
// eslint-disable-next-line no-unused-vars
function dragOverHandler(ev) {
  // Disable default action of browser
  ev.preventDefault();
  const dropZone = document.getElementById('drop-zone');
  dropZone.style.backgroundColor = 'grey';
}

// Reset color of drop-zone when the file leave the zone
// eslint-disable-next-line no-unused-vars
function dragLeaveHandler(ev) {
  // Disable default action of browser
  ev.preventDefault();
  const dropZone = document.getElementById('drop-zone');
  dropZone.style.backgroundColor = 'white';
}

// Add the file to the list when it dropped
// eslint-disable-next-line no-unused-vars
function dropHandler(ev) {
  // Disable default action of browser
  ev.preventDefault();
  const dropZone = document.getElementById('drop-zone');
  dropZone.style.backgroundColor = 'white';

  // Populating files array
  const files = [];

  // Adding files depending of object type used by browser
  if (ev.dataTransfer.items) {
    // New browsers
    for (let i = 0; i < ev.dataTransfer.items.length; i++) {
      if (ev.dataTransfer.items[i].kind === 'file') {
        files.push(ev.dataTransfer.items[i].getAsFile());
      } else {
        alert('Not a file !!');
      }
    }
  } else {
    // Old browsers
    for (let i = 0; i < ev.dataTransfer.files.length; i++) {
      files.push(ev.dataTransfer.files[i]);
    }
  }

  addToList(files);
}

// Submit one file to server
async function uploadFile(file) {
  const data = new FormData();
  data.append('file', file);

  await fetch('/pdf', {
    method: 'POST',
    body: data,
  })
    .then(console.log)
    .catch(console.log);
}

// Uploading files to server
// eslint-disable-next-line no-unused-vars
function uploading() {
  const promises = [];
  filesList.forEach((file) => {
    promises.push(uploadFile(file));
  });
  Promise.all(promises)
    .then(() => {
      alert(`${filesList.length} file(s) uploaded`);
      location.replace('home');
    });
}
